package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.stream.Collectors;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    /**
     * Author name: Andrey Markov
     * */

    public int[][] buildPyramid(List<Integer> inputNumbers) {

        buildable(inputNumbers);
        List<Integer> collect = inputNumbers.stream().sorted().collect(Collectors.toList());
        int a = collect.size();
        int rows = rowNum(a);
        int[][] pyramid = new int[rows][2 * rows - 1];

        for (int row = 0; row < rows; row++) {
            int avail = row + 1;
            int used = 0;
            for (int col = 0; col < (2 * rows - 1); col++) {
                int temp = rows - 1 + used * 2;
                if ((row + col) == temp && collect.size() != 0 && used < avail) {
                    pyramid[row][col] = collect.get(0);
                    collect.remove(0);
                    used++;
                }
            }
        }
        return pyramid;
    }

    private int rowNum(int size) {
        int a = 0;
        for (int i = 1; i < 10000000; i++) {
            a = a + i;
            if (size == a) {
                return i;
            }
        }
        return 0;
    }

    private boolean buildable(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.size() == 0) {
            throw new CannotBuildPyramidException();
        }
        boolean notNulls = false;
        for (Integer a : inputNumbers) {
            if (a == null) {
                throw new CannotBuildPyramidException();
            }
            if (a !=0){
                notNulls = true;
            }
        }
        if(rowNum(inputNumbers.size())== 0 || !notNulls){
            throw new CannotBuildPyramidException();
        }
        return true;
    }
}
