package com.tsystems.javaschool.tasks.subsequence;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Checks if it is possible to get a sequence which is equal to the first
 * one by removing some elements from the second one.
 *
 * @param x first sequence
 * @param y second sequence
 * @return <code>true</code> if possible, otherwise <code>false</code>
 */

/**
 * Author name: Andrey Markov
 * */

@SuppressWarnings("rawtypes")
public class Subsequence {

    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.size() == 0) {
            return true;
        }

        ArrayList<Object> listX = new ArrayList<>();
        ArrayList<Object> listY = new ArrayList<>();
        listX.addAll(x);
        listY.addAll(y);

        for (Object a : listY) {
            if (a.equals(listX.get(0))) {
                listX.remove(0);
                if (listX.size() == 0) {
                    return true;
                }
            }
        }
        return false;
    }
}
