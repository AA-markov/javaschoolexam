package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    /**
     * Author name: Andrey Markov
     * */

    public String evaluate(String statement) {
        try {
            statement = statement.replace(" ", "");
            if (statement == null) {
                return null;
            }
            if (isCalculable(statement)) {
                String strRes = getValue(statement);
                if (strRes == null) {
                    return null;
                }
                double res = Double.parseDouble(strRes);
                if (res - (int) res == 0) {
                    return "" + (int) res;
                }
                return String.valueOf((double) Math.round(res * 10000) / 10000);
            }
            return null;
        } catch (NullPointerException e) {
            return null;
        }
    }

    private boolean isCalculable(String statement) {
        if (statement.isEmpty() || statement.length() < 3 || statement.contains(",")) {
            return false;
        }
        for (int i = 1; i < statement.length(); i++) {
            char a = statement.charAt(i);
            char b = statement.charAt(i - 1);
            if ((a == '-' || a == '+' || a == '/' || a == '*') && (b == '-' || b == '+' || b == '/' || b == '*')) {
                return false;
            }
        }
        Pattern pattern = Pattern.compile("^(?:[-]?\\d+[+-/*])+\\d+$");
        while (statement.contains("(") && statement.contains(")") && (statement.indexOf("(") < statement.indexOf(")"))) {
            String temp = statement.substring(0, statement.indexOf(")"));
            temp = temp.substring(temp.lastIndexOf("(") + 1);
            Matcher mat = pattern.matcher(temp);
            if (mat.matches()) {
                statement = statement.replace("(" + temp + ")", "1");
            } else {
                return false;
            }
        }
        Matcher mat = pattern.matcher(statement);
        return mat.matches();
    }

    private String getValue(String statement) {
        while (statement.contains("(")) {
            String temp = statement.substring(0, statement.indexOf(")"));
            temp = temp.substring(temp.lastIndexOf("(") + 1);
            statement = statement.replace("(" + temp + ")", getValue(temp));
        }
        ArrayList<String> res = new ArrayList<>();
        StringTokenizer strTok = new StringTokenizer(statement, "+*-/", true);
        while (strTok.hasMoreTokens()) {
            res.add(strTok.nextToken());
        }

        for (int i = 0; i < res.size(); i++) {
            if (res.get(i).equals("-")) {
                res.set(i + 1, "-" + res.get(i + 1));
                if (i == 0) {
                    res.remove(i--);
                } else if (res.get(i - 1).equals("/") || res.get(i - 1).equals("*") || res.get(i - 1).equals("-") || res.get(i - 1).equals("+")) {
                    res.remove(i--);
                } else {
                    res.set(i, "+");
                }
            }
        }

        // Multiply & divide
        for (
                int i = 0; i < res.size(); i++) {
            if (res.get(i).equals("*") || res.get(i).equals("/")) {
                if (res.get(i).equals("*")) {
                    res.set(i, (Double.parseDouble(res.get(i - 1)) * Double.parseDouble(res.get(i + 1))) + "");
                } else if (res.get(i).equals("/") && Double.parseDouble(res.get(i + 1)) == 0) {
                    return null;
                } else {
                    res.set(i, (Double.parseDouble(res.get(i - 1)) / Double.parseDouble(res.get(i + 1))) + "");
                }
                res.remove(i - 1);
                res.remove(i--);
            }
        }
        // Plus
        for (int i = 0; i < res.size(); i++) {
            if (res.get(i).equals("+")) {
                res.set(i, (Double.parseDouble(res.get(i - 1)) + Double.parseDouble(res.get(i + 1))) + "");
                res.remove(i - 1);
                res.remove(i--);
            }
        }
        // Minus
        for (int i = 0; i < res.size(); i++) {
            if (res.get(i).equals("-")) {
                res.set(i, (Double.parseDouble(res.get(i + 1)) - Double.parseDouble(res.get(i - 1))) + "");
                res.set(i - 1, "-");
                res.remove(i + 1);
            }
        }
        return res.get(0);
    }
}
